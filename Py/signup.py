####################################################
# What : Home Page script                          #
# Who : Mahesh Thumar                              #                
# When : 23/09/2012                                #            
# How : how to access("/")                         #
# Which : No args as now                           #
####################################################

import webapp2

from google.appengine.api import users
import jinja2

from Py import managesession
from Py import home

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader('HTML'),autoescape=True)

class signup(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user is None:
            user ={"nickname":"Guest"}
            template_values ={}
            template_values["name"] = user["nickname"]
            template = jinja_environment.get_template("signup.html")
            self.response.out.write(template.render(template_values))
        else:
            user1 = managesession.setlogin(self,user)
            self.redirect("/home")
            # user ={"nickname":user.nickname()}
            
        


app = webapp2.WSGIApplication([
  ('/', signup)
], debug=True)
