from gaesessions import get_current_session
from gaesessions import DjangoSessionMiddleware

from google.appengine.api import users

    
def setlogin(self,passuser=None):
    if passuser is None :
        user = users.get_current_user()
    else :
        user = passuser
    if user is not None:
        session = get_current_session()
        session.regenerate_id()
        session['userobj'] = user
        return user
    else :
        # print ""
        # print users.create_login_url("/" ,federated_identity='http://www.google.com/a/itechbit.com')
        # return False
        self.redirect(users.create_login_url("/")) #federated_identity='http://www.google.com/a/itechbit.com')
    
def checklogin(self,redirecttologin=True):
    session = get_current_session()
    try:
        return session['userobj']
    except:
        if redirecttologin:
            self.response.out.write("<script type='text/javascript'>alert('Seems your session is out.')</script>")
            self.redirect(users.create_logout_url("/"))
        else:
            return None
        
def logout(self):
    session = get_current_session()
    del session['userobj']
    session.terminate()
    self.redirect(users.create_logout_url("/"))
    
