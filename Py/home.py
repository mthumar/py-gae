####################################################
# What : Home Page script                          #
# Who : Mahesh Thumar                              #                
# When : 23/09/2012                                #            
# How : how to access("/")                         #
# Which : No args as now                           #
####################################################

import webapp2
import jinja2

from Py import managesession

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader('HTML'),autoescape=True)

class Home(webapp2.RequestHandler):
    def get(self):
        user = managesession.checklogin(self,redirecttologin=False)
        if user is None:
            user = managesession.setlogin(self)
            if user:
                self.get()
        else:
            # self.response.out.write('Hello, ' + user.nickname())
            template_values = {}
            template_values["name"] = user.nickname()
            template = jinja_environment.get_template("home.html")
            self.response.out.write(template.render(template_values))
            
class session(webapp2.RequestHandler):
    def get(self):
        user = managesession.checklogin(self)
        if user:
            self.response.out.write('Hello, ' + user.nickname()+ ' From Session')
        
class logout(webapp2.RequestHandler):
    def get(self):
        managesession.logout(self)

app = webapp2.WSGIApplication([
  ('/home', Home),
  ('/session', session),
  ('/logout', logout)
], debug=True)
